
export function NewImagen(Path,right, down){ // Argumentos

    let fondo = new Image();
    fondo.src = Path; // Insertamos el fondo de pantalla
    fondo.onload = function(){canv.drawImage(fondo,right, down)}
    // Mensaje antibug
    console.log("Se mostró la imagen");
}
export function Hablar(text = "Texto vació" ){
    let synth = window.speechSynthesis
    let utterThis = new SpeechSynthesisUtterance(text)
    synth.speak(utterThis)
}
