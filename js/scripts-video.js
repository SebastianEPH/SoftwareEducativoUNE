/**
 * @fileoverview Menú aprMenu, desplegable con efecto expansión suavizado
 * @version      0.1
 * @author       SebastiánEPH<Sebastianeph99@gmail.com>
 * @copyright    sebastianeph.github.io
 *
 * History
 * v0.1 –
 * ----
 * La primera versión
 */

/**
 * Add a background image
 * @descriptor
 * @param {string} Path Image path.
 * @param {string} Name Image name.
 * @param {integer} Min Ramdowm minimum number
 * @param {integer} Max Ramdom maximum number
 * @param {string} Extension Imagen Extension
 * @param {boolean} Ramdom Active
 * @param {integer} w Number of pixeles uo
 * @param {integer} h Number of pixeles down
 * @constructor
 */
function BackGround(Path,Name,Min,Max= 0,Ramdom = true, w = 1280,h = 720,Extension = "jpg"){ // Argumentos
    var num = Min;
    // Verifica si será Aleatorio o no
    if (Ramdom){num = Math.floor(Math.random()*(Max-Min))+Min;}
    var fondo = new Image(w,h);
    if(Ramdom){
        fondo.src = Path+"/"+Name+" ("+num+")."+Extension; // Insertamos el fondo de pantalla
    }else{
        fondo.src = Path+"/"+Name+"."+Extension; // Insertamos el fondo de pantalla
    }
    fondo.onload = function(){canv.drawImage(fondo,0,0)}
    // Mensaje antibug
    console.log("El Número de fondo es: "+num);
}



/**
 * Plays Sound
 * @param {string} id  ID Unique
 * @constructor
 */
function Sound(id) {
    try {
        document.getElementById(id).play();
        console.log("[Audio] Se reprodución el sonido"+id);
    }catch (e) {
        console.log("[Audio] Error al reproducir"+id+"\n\nError: "+e);
    }
}

// Botones
function Button(id, left, top, link = ""){
    let button = document.getElementById(id);
    button.style.position = "absolute";// relative o absolute
    button.style.left= left+"px";
    button.style.top= top+"px";
    if(link != ""){
        button.onclick = () => document.location.href= link;
        console.log("Se oprimio el botton");
    }else {
        console.log("No se mandaron argumentos a la función")
    }
}

const Texto=(x,y)=>{
    // Texto
    // canv.beginPath();
    let txt = document.getElementById("text-principal");
   // txt.font = "bold 24px verdana";
    txt.style.position = "absolute";// relative o absolute
    txt.style.left= x+"px";
    txt.style.top= y+"px";
}
const Degradado = (color1,color2)=>{
    // Degradado
    let gradiente = canv.createLinearGradient(0,0,1280,720);
    gradiente.addColorStop(0,color1);
    gradiente.addColorStop(1,color2);
    canv.fillStyle = gradiente;  // Muestra gradiente
    canv.fillRect(0,0,1366,768);
}

function Start() {
    // Genera un área de trabajo
    var elemento = document.getElementById("canv");
    canv = elemento.getContext("2d"); // Especifica entorno
    Degradado("#80a1f5","#0e298b")
    // texto
    //Texto(480,45);
    // Botton Home
    Button("button-home",100,640, "../index.html");
    Button("button-video",1100,640, "window-juego-animales.html")
    //video();
}
Start();

// Comienza Script
//window.addEventListener("load", Start, false);