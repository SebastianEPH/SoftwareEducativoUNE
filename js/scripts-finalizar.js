//simport * as hola from "./funciones"
/**
 * Add a background image
 * @descriptor
 * @param {string} Path Image path.
 * @param {string} Name Image name.
 * @param {integer} Min Ramdowm minimum number
 * @param {integer} Max Ramdom maximum number
 * @param {string} Extension Imagen Extension
 * @param {boolean} Ramdom Active
 * @param {integer} w Number of pixeles uo
 * @param {integer} h Number of pixeles down
 * @constructor
 */
function BackGround(Path,Name,Min,Max= 0,Ramdom = true, w = 1280,h = 720,Extension = "jpg"){ // Argumentos
    let  num = Min;
    // Verifica si será Aleatorio o no
    if (Ramdom){num = Math.floor(Math.random()*(Max-Min))+Min;}
    let fondo = new Image(w,h);
    if(Ramdom){
        fondo.src = Path+"/"+Name+" ("+num+")."+Extension; // Insertamos el fondo de pantalla
    }else{
        fondo.src = Path+"/"+Name+"."+Extension; // Insertamos el fondo de pantalla
    }
    fondo.onload = function(){canv.drawImage(fondo,0,0)}
    // Mensaje antibug
    console.log("El Número de fondo es: "+num);
}
/**
 * Add a background image
 * @descriptor
 * @param {string} Path Image path.
 * @param {string} Name Image name.
 * @param {integer} Min Ramdowm minimum number
 * @param {integer} Max Ramdom maximum number
 * @param {string} Extension Imagen Extension
 * @param {boolean} Ramdom Active
 * @param {integer} w Number of pixeles uo
 * @param {integer} h Number of pixeles down
 * @constructor
 */

function Hablar(text = "Texto vació" ){
    let synth = window.speechSynthesis
    let utterThis = new SpeechSynthesisUtterance(text)
    synth.speak(utterThis)
}

/**
 * Plays Sound
 * @param {string} id  ID Unique
 * @constructor
 */
function Sound(id) {
    try {
        document.getElementById(id).play();
        console.log("[Audio] Se reprodución el sonido"+id);
    }catch (e) {
        console.log("[Audio] Error al reproducir"+id+"\n\nError: "+e);
    }
}
// Botones
function Button(id, right, down, link = ""){
    let button = document.getElementById(id);
    button.style.position = "absolute";// relative o absolute
    button.style.left= right+"px";
    button.style.top= down+"px";
    if(link != ""){
        button.onclick = () => document.location.href= link;
        console.log("Se oprimio el botton");
    }else {
        console.log("No se mandaron argumentos a la función")
    }
}
const Texto=(id,x,y)=>{
    // Texto
    // canv.beginPath();
    let $txt = document.getElementById(id);
    // txt.font = "bold 24px verdana";
    $txt.style.position = "absolute";// relative o absolute
    $txt.style.left= x+"px";
    $txt.style.top= y+"px";
}

function Degradado (color1,color2){
    // Degradado
    let gradiente = canv.createLinearGradient(0,0,1366,768);
    gradiente.addColorStop(0,color1);
    gradiente.addColorStop(1,color2);
    canv.fillStyle = gradiente;  // Muestra gradiente
    canv.fillRect(0,0,1366,768);
}
function Start() {
    let elemento = document.getElementById("canv");
    canv = elemento.getContext("2d"); // Especifica entorno

    Texto("text-principal",185,125);
    Texto("text-principal2",450,190);
    // agregar imagen
    // BackGround("img/menu","fondo",1,11);
    Degradado("#ffffff","#740e8b")

    //NewImagen("../img/gif/pato.gif",450,200)

    // Sonido de fondo
   // Sound("SoundBackground");
    //Button("buttonPrincipal",645,780,"../window-menu.html")


    Hablar("Hasta en otra oportunidad mis queridos estudiantes... pueden salir al recreo")


}

// Comienza Script
window.addEventListener("load", Start, false);