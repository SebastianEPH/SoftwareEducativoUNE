# SoftwareEducativoUNE

Software Eductivo para la UNE
# Caracteristicas

* __Developed by:__ `SebastianEPH`
* __Product name:__ `Software Educativo - CTA `
* __Type software:__ `WEB - Software Educativo`
* __File version:__ `None`
* __State:__ `Alfa`
* __Size:__ `400KB aprox`
* __Plataform:__ `WEB [Chrome-Mozilla-Firefox-Opera]`
* __Programming language:__ `HTML, CSS, Js`
* __Licence:__ `MIT`
* __IDE or text editor:__ `Visual Studio Code`
* __Documentation date:__ `08/06/2020`
* __Description:__ `Software Educativo creado con la finalidad`

# Proceso de Web 

__LINK:__ [sebastianeph.gitlab.io/SoftwareEducativoUNE](https://sebastianeph.gitlab.io/SoftwareEducativoUNE/)

# Detalles generales del Software:

* __Servidor:__
    * Nombre: `Gitlab`
    * Costo: `Gratuito`
* __Tecnologías utilizadas:__
    * Lenguaje: `HTML` > Básico
    * Lenguaje: `JavaScript` > Básico 
    * Lenguaje: `CSS`> Básico

* __Fiabilidad requerida :__ `Básico`




# Desarrollo por fechas
## Fecha: `18/06/2020`

![Fecha 18/06/2020](https://i.imgur.com/5N2ZfMG.png)